from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from rest_framework_api_key.models import AbstractAPIKey
from django.dispatch import receiver
from django.db.models.signals import post_save

from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from .managers import CustomUserManager, CustomAPIKeyManager

class Client(models.Model):
	name = models.CharField(max_length=100)
	is_active = models.BooleanField(default=True)

	class Meta:
		verbose_name = "Client"
		verbose_name_plural = "Clients"

class CustomAPIKey(AbstractAPIKey):
	client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='api_keys')
	api_key = models.CharField(max_length=100, verbose_name='API Key', null=True, blank=True, unique=True)

	objects = CustomAPIKeyManager()

	class Meta(AbstractAPIKey.Meta):
		verbose_name = "Client API key"
		verbose_name_plural = "Client API keys"

class CustomUser(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(_('email address'), unique=True)
	is_staff = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	date_joined = models.DateTimeField(default=timezone.now)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	objects = CustomUserManager()

	def __str__(self):
		return self.email

	class Meta:
		verbose_name = "User"
		verbose_name_plural = "Users"

class Posts (models.Model):

	title = models.CharField(max_length=250)
	content = models.TextField()

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = "Post"
		verbose_name_plural = "Posts"

@receiver(post_save, sender=Client)
def create_client_api_key(sender, instance, created, **kwargs):
	if created:
		name = instance.name + ' API KEY'
		api_key, key = CustomAPIKey.objects.create_key(name=name, client=instance)
		api_key.api_key = key
		api_key.save()