from rest_framework_api_key.permissions import BaseHasAPIKey
from .models import CustomAPIKey

class HasClientAPIKey(BaseHasAPIKey):
    model = CustomAPIKey