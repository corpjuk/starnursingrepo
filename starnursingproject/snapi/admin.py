from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, CustomAPIKey, Client
from rest_framework_api_key.admin import APIKeyModelAdmin

class CustomUserAdmin(UserAdmin):
	add_form = CustomUserCreationForm
	form = CustomUserChangeForm
	model = CustomUser
	list_display = ('email', 'is_staff', 'is_active',)
	list_filter = ('email', 'is_staff', 'is_active',)
	fieldsets = (
		(None, {'fields': ('email', 'password')}),
		('Permissions', {'fields': ('is_staff', 'is_active')}),
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
		),
	)
	search_fields = ('email',)
	ordering = ('email',)

class CustomAPIKeyAdmin(APIKeyModelAdmin):
	list_display = (*APIKeyModelAdmin.list_display, "api_key", "get_client_name")
	search_fields = (*APIKeyModelAdmin.search_fields, "api_key", "get_client_name")

	def get_client_name(self, obj):
		return  obj.client.name
	get_client_name.short_description = 'Client Name'
	get_client_name.admin_order_field = 'client__name'

class APIKeyInline(admin.TabularInline):
	model = CustomAPIKey
	extra = 0	
	max_num = 3

	def get_extra (self, request, obj=None, **kwargs):
		"""Dynamically sets the number of extra forms. 0 if the related object
		already exists or the extra configuration otherwise."""
		if obj:
			return 0
		return self.extra

class ClientAdmin(admin.ModelAdmin):
	inlines  = [
		APIKeyInline,
	]

admin.site.register(Client, ClientAdmin)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(CustomAPIKey, CustomAPIKeyAdmin)