from .base import *

DEBUG = True

IS_TESTING = True

TEMPLATES[0]['OPTIONS']['debug'] = True

IS_LOCAL = True

ALLOWED_HOSTS = ['*']

DATABASES = {

    'default': {

        'ENGINE': 'django.db.backends.postgresql_psycopg2',

        'NAME': 'sndb',

        'USER': 'sndbuser',

        'PASSWORD': 'secret',

        'HOST': 'sndb',

        'PORT': '5432',

    }

}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'WARNING',
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'propagate': False,
        },
    },
}